## O:

Today I learned tasking and context map

## R：

I feel it is useful

## I：

I think by breaking down a requirement into small tasking, and then by drawing a context map to clarify the input and output of each part of the method can effectively understand the underlying structure of the requirement. Thus, it is better to fulfill the requirements.

## D:

In the future, when you encounter a requirement, you will not be in a hurry to write code, but first split into specific methods and then through the conversation context map to clarify the role of each method and the order.