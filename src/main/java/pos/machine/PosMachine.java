package pos.machine;
import java.util.*;
import java.util.stream.Collectors;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        ArrayList<ResultItem> resultItems = countBarcodeNum(barcodes);
        String s = makeRecords(resultItems);
        return s;
    }


    public ArrayList<ResultItem> countBarcodeNum(List<String> bc) {

        List<Item> items = new ItemsLoader().loadAllItems();
        String s = items.toString();
        Map<String, Long> frequencyMap = bc.stream()
                .collect(Collectors.groupingBy(e -> e, Collectors.counting()));
        Map<String, Long> barcodesInfoMap = new HashMap<>();

        frequencyMap.forEach((barcode, count) -> barcodesInfoMap.put(barcode, count));
        List<String> barcodes = toDealList(bc);
        ArrayList<ResultItem> resultItems = new ArrayList<>();

        for (String barcode : barcodes) {
            Item item = selectItem(barcode);
            ResultItem resultItem = new ResultItem(item.getBarcode(), item.getName(), item.getPrice(), Math.toIntExact(barcodesInfoMap.get(barcode)));

            resultItems.add(resultItem);
        }
        return resultItems;
    }
    public Item selectItem(String barcode) {
        ItemsLoader itemsLoader = new ItemsLoader();
        List<Item> itemList = itemsLoader.loadAllItems();
        for (Item item : itemList) {
            if (item.getBarcode().equals(barcode)) return item;
        }
        return null;
    }
    public static List<String> toDealList(List<String> barcodes) {
        Set<String> set = new TreeSet<>(barcodes);
        List<String> newList = new ArrayList<>(set);
        return newList;
    }
    public String makeRecords(List<ResultItem> resultItems) {
        StringBuilder makeRecords = new StringBuilder();
        StringBuilder itemRecords = new StringBuilder();
        int total = 0;
        for (ResultItem reciptItem : resultItems) {
            total += reciptItem.getPrice() * reciptItem.getCount();
            itemRecords.append(makeLine(reciptItem)).append("\n");
        }
        return makeRecords.append("***<store earning no money>Receipt***").append("\n").append(itemRecords).append("----------------------").append("\n")
                .append("Total: ").append(total).append(" (yuan)").append("\n").append("**********************").toString();
    }

    public String makeLine(ResultItem resultItem) {
        StringBuilder generateLine = new StringBuilder();
        return generateLine.append("Name: ").append(resultItem.getName()).append(", ").append("Quantity: ").append(resultItem.getCount()).append(", ")
                .append("Unit price: ").append(resultItem.getPrice()).append(" (yuan)").append(", ").append("Subtotal: ").append(resultItem.getPrice() * resultItem.getCount())
                .append(" (yuan)").toString();
    }



}
