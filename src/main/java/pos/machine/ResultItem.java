package pos.machine;

public class ResultItem {
    private final String barcode;
    private final String name;
    private final int price;
    private final long count;



    public ResultItem(String barcode, String name, int price, int count) {
        this.barcode = barcode;
        this.name = name;
        this.price = price;
        this.count = count;
    }

    public String getBarcode() {
        return barcode;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public long getCount() {
        return count;
    }

    @Override
    public String toString() {
        return "ResultItem{" +
                "barcode='" + barcode + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", count=" + count +
                '}';
    }
}
